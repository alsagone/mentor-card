#!/bin/bash

function webpThis {
	new_name="${1%.*}.webp"
	cwebp -q 100 "$1" -resize 0 1080 -o "$new_name"
}

for i in *.png *.jpg *.jpeg
do
	webpThis "$i"
done
