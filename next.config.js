/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  basePath: "/mentor-card",
  images: {
    unoptimized: true,
  },
};

module.exports = nextConfig;
