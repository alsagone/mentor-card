import React from "react";
import Introduction from "../components/en/intro";
import StarPlayer from "../components/en/starPlayer";
import Invites from "../components/en/invites";
import Projects from "../components/en/projects";
import Head from "next/head";
import Image from "next/image";
import styles from "../styles/Home.module.css";
import { EnglishButton } from "../components/btnRouter";

export default function English() {
  return (
    <div className={styles.container}>
      <div id="background-image" />
      <Head>
        <title>alsagone - Mentor Card</title>
        <meta
          name="description"
          content="Projects made by alsagone, an Ubisoft Star Player"
        />
        <link rel="icon" href="/mentor-card/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <div id="container">
          <h1 className="title">alsagone - Ubisoft Star Player</h1>
          <EnglishButton />
          <Introduction />
          <StarPlayer />
          <Projects />
          <Invites />
        </div>
      </main>
    </div>
  );
}
