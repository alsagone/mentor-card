import React from "react";
import English from "./en";
import French from "./fr";

interface MyProps {}

interface MyState {
  french: boolean | undefined;
}

export default class App extends React.Component<MyProps, MyState> {
  constructor(props: MyProps) {
    super(props);
    this.state = { french: undefined };
  }

  componentDidMount() {
    const userPreferredLanguage: string = navigator.language.toLowerCase();
    const userIsFrench: boolean = userPreferredLanguage.startsWith("fr");
    this.setState({ french: userIsFrench });
  }

  render(): React.ReactNode {
    return this.state.french ? <French /> : <English />;
  }
}
