import "../styles/globals.css";

import "../styles/intro.css";
import "semantic-ui-css/semantic.min.css";
import "../styles/section.css";
import "../styles/projects.css";
import "../styles/buttons.css";

import type { AppProps } from "next/app";

export default function App({ Component, pageProps }: AppProps) {
  return <Component {...pageProps} />;
}
