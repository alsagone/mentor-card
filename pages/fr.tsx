import React from "react";
import Introduction from "../components/fr/intro";
import StarPlayer from "../components/fr/starPlayer";
import Invites from "../components/fr/invites";
import Projects from "../components/fr/projects";
import Head from "next/head";
import Image from "next/image";
import styles from "../styles/Home.module.css";
import { FrenchButton } from "../components/btnRouter";

export default function French() {
  return (
    <div className={styles.container}>
      <div id="background-image" />
      <Head>
        <title>alsagone - Mentor Card</title>
        <meta
          name="description"
          content="Projects made by alsagone, an Ubisoft Star Player"
        />
        <link rel="icon" href="/mentor-card/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <div id="container">
          <h1 className="title">alsagone - Star Player Ubisoft</h1>
          <FrenchButton />
          <Introduction />
          <StarPlayer />
          <Projects />
          <Invites />
        </div>
      </main>
    </div>
  );
}
