import React from "react";
import { Image } from "semantic-ui-react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { IconProp } from "@fortawesome/fontawesome-svg-core";
import { faWindowRestore } from "@fortawesome/free-solid-svg-icons";
import { Project } from "./projectList";

interface MyProps {
  language: string;
  project: Project;
}

interface MyState {}

export class ProjectCard extends React.Component<MyProps, MyState> {
  constructor(props: MyProps) {
    super(props);
  }

  render(): React.ReactNode {
    let title: string, description: string;

    if (this.props.language === "fr") {
      title = this.props.project.title.fr;
      description = this.props.project.description.fr;
    } else {
      title = this.props.project.title.en;
      description = this.props.project.description.en;
    }

    const picture: string = `images/projects/${this.props.project.picture}.webp`;

    return (
      <div className="presentation">
        <Image src={picture} alt={title} />

        <div className="project-attributes">
          <span className="project-title">{title}</span>
          <span className="project-description">{description}</span>

          <span className="social">
            <span className="buttons">
              <a
                href={this.props.project.url}
                rel="noreferrer norelopener"
                title={title}
                target="_blank"
              >
                <FontAwesomeIcon icon={faWindowRestore as IconProp} />
              </a>
            </span>
          </span>
        </div>
      </div>
    );
  }
}
