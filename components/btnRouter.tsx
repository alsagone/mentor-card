import { useRouter } from "next/router";
import React from "react";
import { Button } from "semantic-ui-react";

export const EnglishButton = () => {
  const router = useRouter();

  return (
    <Button.Group>
      <Button active>English</Button>
      <Button.Or />
      <Button
        active={false}
        onClick={(e) => {
          e?.preventDefault;
          router.push("/fr");
        }}
      >
        Français
      </Button>
    </Button.Group>
  );
};

export const FrenchButton = () => {
  const router = useRouter();

  return (
    <Button.Group>
      <Button
        active={false}
        onClick={(e) => {
          e?.preventDefault;
          router.push("/en");
        }}
      >
        English
      </Button>
      <Button.Or />
      <Button active>Français</Button>
    </Button.Group>
  );
};
