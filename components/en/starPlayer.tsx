import React from "react";
import { CardGroup } from "semantic-ui-react";
import { PictureCard, Picture } from "../Picture";
import picturesList from "../pictureList";
import links from "../links";
const pictures: Picture[] = picturesList.filter(
  (element) => element.section === "StarPlayer"
);

const StarPlayer = () => {
  return (
    <div className="section">
      <span className="section-title ">
        What does being a Star Player mean?
      </span>
      <div className="section-content">
        <div className="section-text ">
          <p>
            Ubisoft published an extensive{" "}
            <a
              href={links.blogPost.en}
              target="_blank"
              rel="noopener noreferrer"
            >
              blogpost
            </a>{" "}
            about all the Star Player activities if you want to know more about
            it, but I&apos;ll try to summarize it shortly. <br />
            The <span className="bold">Star Player program</span> has been
            created by Ubisoft to{" "}
            <span className="italic">
              {" "}
              &quot;act as a liaision between Ubisoft and its most dedicated
              fans&quot;
            </span>
            . Sometimes, we get invited to conventions - lately we had the
            chance to get invited to Ubisoft Paris&apos; studio to watch the
            Ubisoft Forward conference (staying at a 4-star hotel for a night is
            quite a life-changing experience!), but we also have a direct line
            of contact with various Community Developers who gather our feedback
            and relay it to the teams. We also got involved directly with the
            creation of in-game content, for example back in 2021, we had the
            chance to create{" "}
            <a href={links.swords.en} target="_blank" rel="noopener noreferrer">
              two swords
            </a>{" "}
            for Assassin&apos;s Creed Valhalla.
          </p>
        </div>
      </div>
      <div className="pictures ">
        {pictures.map((element) => (
          <PictureCard language="en" key={element.file} picture={element} />
        ))}
      </div>
    </div>
  );
};

export default StarPlayer;
