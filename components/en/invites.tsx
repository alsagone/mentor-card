import React from "react";
import { Picture, PictureCard } from "../Picture";
import pictureList from "../pictureList";
import links from "../links";

const pgw_pictures: Picture[] = [];
const ubiXP_pictures: Picture[] = [];
const ubiFwd_pictures: Picture[] = [];

pictureList.forEach((p) => {
  if (p.section === "PGW") {
    pgw_pictures.push(p);
  } else if (p.section === "UbiXP") {
    ubiXP_pictures.push(p);
  } else if (p.section === "UbiFwd") {
    ubiFwd_pictures.push(p);
  }
});

const Invites = () => {
  return (
    <div className="section">
      <span className="section-title ">Where did you get invited to?</span>
      <div className="section-content">
        <div className="section-text ">
          <div className="event">
            <p>
              <span className="bold">The 2017&apos;s Paris Games Week</span>
              <br />
              aka the biggest French videogame convention - we attended the
              Press night, the day before the official opening to the public -
              meaning we could try all games without having to wait in a line
              for two hours :D
              <br />
              The next day, we had a full tour of Ubisoft&apos;s Paris studio !
            </p>
            <div className="pictures ">
              {pgw_pictures.map((element) => (
                <PictureCard
                  language="en"
                  key={element.file}
                  picture={element}
                />
              ))}
            </div>
          </div>

          <div className="event">
            <p>
              <span className="bold">The Ubisoft Experience (2019)</span>
              <br /> Link to the{" "}
              <a href={links.ubiExp} target="_blank" rel="noopener noreferrer">
                YouTube video
              </a>{" "}
              of the event
            </p>
            <div className="pictures">
              {ubiXP_pictures.map((element) => (
                <PictureCard
                  language="en"
                  key={element.file}
                  picture={element}
                />
              ))}
            </div>
          </div>
          <div className="event">
            <p>
              <span className="bold">L&apos;Ubisoft Forward (2022)</span>
              <br />
              You can rewatch the confrence{" "}
              <a
                href={links.ubiFwd.en}
                target="_blank"
                rel="noopener noreferrer"
              >
                here
              </a>{" "}
              and you can also watch{" "}
              <a
                href={links.ubiFwd.mirage}
                target="_blank"
                rel="noopener noreferrer"
              >
                this video
              </a>{" "}
              of the crowd&apos;s reaction to Assassin&apos;s Creed
              Mirage&apos;s trailer.
            </p>
            <div className="pictures ">
              {ubiFwd_pictures.map((element) => (
                <PictureCard
                  language="en"
                  key={element.file}
                  picture={element}
                />
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Invites;
