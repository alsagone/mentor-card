import React from "react";
import { projectList } from "../projectList";
import { ProjectCard } from "../project";
const Projects = () => {
  return (
    <div className="section">
      <span className="section-title ">My projects</span>
      <div className="section-content">
        <div className="section-text">
          {projectList.map((element) => (
            <ProjectCard
              key={element.title.en}
              language="en"
              project={element}
            />
          ))}
        </div>
      </div>
    </div>
  );
};

export default Projects;
