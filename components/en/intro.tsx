import React from "react";
import ProfileCard from "../en/profileCard";
const Introduction = () => {
  return (
    <div className="section">
      <span className="section-title">Introduction</span>
      <div className="section-content">
        <ProfileCard />
        <p className="section-text intro ">
          Hi, I&apos;m Hakim (aka alsagone) ! <br />
          I&apos;m a Ubisoft Star Player since 2017 and I joined the Mentors
          Guild program in 2020 (before it closed in 2022). I made this website
          to showcase my &quot;Ubisoft-related&quot; projects and also explain
          how I somehow got here. It&apos;s way easier to do that on a website
          than out loud in a conversation because I always think I sound
          extrememly cocky when I explain what I do with Ubisoft :D
        </p>
      </div>
    </div>
  );
};

export default Introduction;
