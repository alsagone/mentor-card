import React from "react";
import { Card, Image } from "semantic-ui-react";

export interface Picture {
  file: string;
  alt: { fr: string; en: string };
  caption: { fr: string; en: string };
}

interface MyProps {
  language: string;
  picture: Picture;
}

interface MyState {}

export class PictureCard extends React.Component<MyProps, MyState> {
  constructor(props: MyProps) {
    super(props);
  }

  render(): React.ReactNode {
    const file: string = `images/${this.props.picture.file}.webp`;
    let caption: string, alt: string;

    if (this.props.language === "fr") {
      caption = this.props.picture.caption.fr;
      alt = this.props.picture.alt.fr;
    } else {
      caption = this.props.picture.caption.en;
      alt = this.props.picture.alt.en;
    }

    return (
      <Card>
        <Image src={file} alt={alt} />
        <Card.Content>
          <Card.Description>{caption}</Card.Description>
        </Card.Content>
      </Card>
    );
  }
}
