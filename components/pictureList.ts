const pictureList = [
  {
    file: "ubi_fwd",
    alt: {
      fr: "Photo avec les Star Players français devant la statue d'Ezio chez Ubisoft Paris",
      en: "French Star Players taking a picture in front of the Ezio statue at Ubisoft Paris' studio",
    },
    caption: {
      fr: "Photo avec les Star Players français devant la statue d'Ezio chez Ubisoft Paris - c'est moi avec le drapeau français Assassin's Creed Unity :)",
      en: "French Star Players in front of the Ezio statue at Ubisoft Paris' studio - I'm the guy with the Assassin's Creed Unity themed French flag :)",
    },
    section: "StarPlayer",
  },
  {
    file: "SP-AC-LOGO",
    alt: {
      fr: "Logo du Mentors' Order",
      en: "Logo of the Mentors' Order",
    },
    caption: {
      fr: "Logo du Mentors Order: le sous-groupe des Star Players consacré à Assassin's Creed",
      en: "Logo of the Mentors Order: the subset of Star Players focused on Assassin's Creed",
    },
    section: "StarPlayer",
  },
  {
    file: "Carte_visite",
    alt: {
      fr: "Ma carte de visite",
      en: "My business card",
    },
    caption: {
      fr: "Ma carte de visite",
      en: "My business card",
    },
    section: "StarPlayer",
  },
  {
    file: "invites/pgw-1",
    alt: {
      fr: "Stand Assassin's Creed Origins à la Paris Games Week 2017",
      en: "Assassin's Creed Origins' booth at the Paris Games Week 2017",
    },
    caption: {
      fr: "Stand Assassin's Creed Origins à la Paris Games Week 2017",
      en: "Assassin's Creed Origins' booth at the Paris Games Week 2017",
    },
    section: "PGW",
  },
  {
    file: "invites/pgw-3",
    alt: {
      fr: "L'entrée de l'ancien studio d'Ubisoft à Montreuil",
      en: "The entrance of the now former Ubisoft studio in Paris",
    },
    caption: {
      fr: "L'entrée de l'ancien studio d'Ubisoft à Montreuil",
      en: "The entrance of the now former Ubisoft studio in Paris",
    },
    section: "PGW",
  },
  {
    file: "invites/pgw-4",
    alt: {
      fr: "La pièce principale où on a passé l'après-midi à jouer à Just Dance 2017 et Assassin's Creed Origins",
      en: "The main room where we spent the afternoon playing Just Dance 2017 and Assassin's Creed Origins",
    },
    caption: {
      fr: "La pièce principale où on a passé l'après-midi à jouer à Just Dance 2017 et Assassin's Creed Origins",
      en: "The main room where we spent the afternoon playing Just Dance 2017 and Assassin's Creed Origins",
    },
    section: "PGW",
  },

  {
    file: "invites/ubixp-1",
    alt: {
      fr: "La scène de l'Ubisoft Forward",
      en: "The Ubisoft Forward's stage",
    },
    caption: {
      fr: "La scène de l'Ubisoft Forward",
      en: "The Ubisoft Forward's stage",
    },
    section: "UbiXP",
  },
  {
    file: "invites/ubixp-2",
    alt: {
      fr: "Selfie devant le stand Rainbow Six Siege",
      en: "Selfie in front of the Rainbow Six Siege booth",
    },
    caption: {
      fr: "Selfie devant le stand Rainbow Six Siege",
      en: "Selfie in front of the Rainbow Six Siege booth",
    },
    section: "UbiXP",
  },
  {
    file: "invites/ubifwd-1",
    alt: {
      fr: "Selfie devant l'entrée de l'Ubisoft Forward",
      en: "Selfie in front of the Ubisoft Forward's entrance",
    },
    caption: {
      fr: "Selfie devant l'entrée de l'Ubisoft Forward",
      en: "Selfie in front of the Ubisoft Forward's entrance",
    },
    section: "UbiFwd",
  },
  {
    file: "invites/ubifwd-2",
    alt: {
      fr: "Compte à rebours du début de la conférence avec Rick Boer en cosplay de Basim (AC Mirage)",
      en: "Countdown to the start of the confrence - featuring Rick Boer cosplaying Basim (AC Mirage)",
    },
    caption: {
      fr: "Compte à rebours du début de la conférence avec Rick Boer en cosplay de Basim (AC Mirage)",
      en: "Countdown to the start of the confrence - featuring Rick Boer cosplaying Basim (AC Mirage)",
    },
    section: "UbiFwd",
  },
  {
    file: "invites/ubifwd-3",
    alt: {
      fr: "Photo avec Yves Guillemot, PDG et co-fondateur d'Ubisoft",
      en: "Picture with Yves Guillemot, CEO and co-founder of Ubisoft",
    },
    caption: {
      fr: "Photo avec Yves Guillemot, PDG et co-fondateur d'Ubisoft",
      en: "Picture with Yves Guillemot, CEO and co-founder of Ubisoft",
    },
    section: "UbiFwd",
  },
];

export default pictureList;
