export interface Project {
  title: { en: string; fr: string };
  url: string;
  description: { en: string; fr: string };
  picture: string;
}

export const projectList: Project[] = [
  {
    title: { en: "Assassin's Creed Guide", fr: "Guide Assassin's Creed" },
    url: "https://tiny.cc/GuideAC",
    picture: "guide_ac",
    description: {
      fr: "Google Doc de plus de 360 pages - que j'ai co-écrit avec mon amie @PoisoonB - qui résume tous les éléments de lore des Assassin's Creed et tous les jeux principaux",
      en: "A 360+ pages long Google Doc I wrote with my friend @PoisoonB that summarizes every main Assassin's Creed main entry and the entire saga's lore. Sadly for you, it's in French but who knows, someday we'll have the courage to translate it into English :D",
    },
  },
  {
    title: {
      en: "Rainbow Six Siege Quick Guide",
      fr: "Guide Rainbow Six Siege",
    },
    picture: "r6",
    url: "https://tiny.cc/R6QuickGuide",
    description: {
      fr: "Site qui liste tous les opérateurs de Rainbow Six Siege avec une description de leur(s) capacité(s) et/ou leur gadget",
      en: "Website where you can get a description of every Rainbow Six Siege's operator's ability/gadget",
    },
  },
  {
    title: {
      en: "Assassin's Creed wallpaper",
      fr: "Fond d'écran Assassin's Creed",
    },
    picture: "ac_stripes",
    url: "https://tiny.cc/AC_Stripes",
    description: {
      fr: "Fond d'écran avec tous les personnages d'Assassin's Creed - plusieurs combinaisons possibles",
      en: "Wallpaper featuring every main Assassin's Creed character - several combinations possible",
    },
  },

  {
    title: { en: "Gaming card", fr: "Gaming card" },
    picture: "gaming_card",
    url: "https://tiny.cc/gaming-card",
    description: {
      fr: "Site qui permet de générer une image qui regroupe tous vos pseudos sur différentes plateformes",
      en: "Website where you can get a picture showcasing all your socials",
    },
  },
];
