import React from "react";
import { Picture, PictureCard } from "../Picture";
import pictureList from "../pictureList";
import links from "../links";

const pgw_pictures: Picture[] = [];
const ubiXP_pictures: Picture[] = [];
const ubiFwd_pictures: Picture[] = [];

pictureList.forEach((p) => {
  if (p.section === "PGW") {
    pgw_pictures.push(p);
  } else if (p.section === "UbiXP") {
    ubiXP_pictures.push(p);
  } else if (p.section === "UbiFwd") {
    ubiFwd_pictures.push(p);
  }
});

const Invites = () => {
  return (
    <div className="section">
      <span className="section-title">Où as-tu été invité?</span>
      <div className="section-content">
        <div className="section-text">
          <div className="event">
            <p>
              <span className="bold">La Paris Games Week 2017</span>
              <br />
              On a pu faire la soirée Presse (et donc pouvoir tester tous les
              jeux sans aucune file d&apos;attente), on a eu aussi la chance de
              pouvoir faire un tour dans le (maintenant ancien) studio
              d&apos;Ubisoft à Montreuil !
            </p>
            <div className="pictures">
              {pgw_pictures.map((element) => (
                <PictureCard
                  language="fr"
                  key={element.file}
                  picture={element}
                />
              ))}
            </div>
          </div>

          <div className="event">
            <p>
              <span className="bold">L&apos;Ubisoft Experience (2019)</span>
              <br /> Lien vers la{" "}
              <a href={links.ubiExp} target="_blank" rel="noopener noreferrer">
                vidéo YouTube
              </a>{" "}
              récapitulant l&apos;événement
            </p>
            <div className="pictures">
              {ubiXP_pictures.map((element) => (
                <PictureCard
                  language="fr"
                  key={element.file}
                  picture={element}
                />
              ))}
            </div>
          </div>
          <div className="event">
            <p>
              <span className="bold">L&apos;Ubisoft Forward (2022)</span>
              <br />
              Vous pouvez revoir la conférence{" "}
              <a
                href={links.ubiFwd.fr}
                target="_blank"
                rel="noopener noreferrer"
              >
                ici
              </a>{" "}
              et aussi regarder{" "}
              <a
                href={links.ubiFwd.mirage}
                target="_blank"
                rel="noopener noreferrer"
              >
                cette vidéo
              </a>{" "}
              des réactions de la foule au trailer d&apos; Assassin&apos;s Creed
              Mirage.
            </p>
            <div className="pictures">
              {ubiFwd_pictures.map((element) => (
                <PictureCard
                  language="fr"
                  key={element.file}
                  picture={element}
                />
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Invites;
