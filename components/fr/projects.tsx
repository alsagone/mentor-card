import React from "react";
import { projectList } from "../projectList";
import { ProjectCard } from "../project";
const Projects = () => {
  return (
    <div className="section">
      <span className="section-title">Mes projets</span>
      <div className="section-content">
        <div className="section-text">
          {projectList.map((element) => (
            <ProjectCard key={element.url} language="fr" project={element} />
          ))}
        </div>
      </div>
    </div>
  );
};

export default Projects;
