import React from "react";
import ProfileCard from "../fr/profileCard";
const Introduction = () => {
  return (
    <div className="section">
      <span className="section-title">Introduction</span>
      <div className="section-content">
        <ProfileCard />
        <div className="section-text intro">
          <p>
            Bonjour, je suis Hakim (aka alsagone) ! <br />
            Je suis Star Player Ubisoft depuis 2017 et j&apos;ai rejoint le
            programme Mentors Guild en 2020 (avant sa fermeture en 2022).
            J&apos;ai fait ce site pour lister mes projets plus ou moins reliés
            à Assassin&apos;s Creed et d&apos;autres jeux Ubisoft et aussi
            expliquer un peu comment j&apos;en suis arrivé là !<br />
            C&apos;est plus facile de faire ça sur un site Internet comparé à
            une conversation parce que j&apos;ai toujours l&apos;impression de
            paraître très prétentieux quand j&apos;essaie d&apos;expliquer ce
            que je fais avec Ubisoft :D
          </p>
        </div>
      </div>
    </div>
  );
};

export default Introduction;
