import React from "react";
import Image from "next/image";
import { IconProp } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faInstagram,
  faTwitter,
  faMastodon,
  faBluesky,
  faTwitch,
} from "@fortawesome/free-brands-svg-icons";

const getYearDiff = (date1: Date, date2: Date): number => {
  const startDate = date1 < date2 ? date1 : date2;
  const endDate = startDate === date1 ? date2 : date1;
  const ms = endDate.getTime() - startDate.getTime();

  return Math.abs(new Date(ms).getUTCFullYear() - 1970);
};

const age: number = getYearDiff(new Date(1995, 10, 9), new Date());

const ProfileCard = () => {
  return (
    <div id="profile-card">
      <Image
        className="card__image"
        src="/mentor-card/images/profilePicture.webp"
        alt="Profile picture"
        width={200}
        height={200}
      />

      <div id="infos">
        <ul>
          <li>Hakim/alsagone</li>
          <li>{age} ans</li>
          <li>Dev web</li>
          <li>🏳️‍🌈 Aroace</li>
        </ul>
      </div>

      <div className="social">
        <div className="buttons">
          <a
            href="https://twitter.com/alsagone"
            target="_blank"
            rel="noreferrer"
            title="Mon compte Twitter"
          >
            <FontAwesomeIcon icon={faTwitter as IconProp} />
          </a>

          <a
            href="https://bsky.app/profile/alsagone.bsky.social"
            target="_blank"
            rel="noreferrer"
            title="Mon profil Bluesky"
          >
            <FontAwesomeIcon icon={faBluesky} />
          </a>

          <a
            href="https://mastodon.social/@alsagone"
            target="_blank"
            rel="noreferrer"
            title="Mon profil Mastodon"
          >
            <FontAwesomeIcon icon={faMastodon as IconProp} />
          </a>

          <a
            href="https://twitch.tv/alsagone"
            target="_blank"
            rel="noreferrer"
            title="Ma chaîne Twitch"
          >
            <FontAwesomeIcon icon={faTwitch} />
          </a>

          <a
            href="https://instagram.com/alsagone67"
            target="_blank"
            rel="noreferrer"
            title="Mon profil Instagram"
          >
            <FontAwesomeIcon icon={faInstagram as IconProp} />
          </a>
        </div>

        <div className="flex flex-col">
          <a
            href="https://ko-fi.com/alsagone"
            target="_blank"
            rel="noreferrer"
            title="Mon profil Ko-Fi"
          >
            <Image
              id="ko-fi"
              src="/mentor-card/images/ko-fi_transparent.webp"
              alt="Buy Me a Coffee at ko-fi.com"
              height={36}
              width={182}
            />
          </a>
        </div>
      </div>
    </div>
  );
};

export default ProfileCard;
