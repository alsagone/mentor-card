import React from "react";
import { PictureCard, Picture } from "../Picture";
import pictureList from "../pictureList";
import links from "../links";

const pictures: Picture[] = pictureList.filter(
  (element) => element.section === "StarPlayer"
);

const StarPlayer = () => {
  return (
    <div className="section">
      <span className="section-title">
        Qu&apos;est-ce qu&apos;un Star Player?
      </span>
      <div className="section-content">
        <div className="section-text">
          <p>
            Ubisoft a publié un{" "}
            <a
              href={links.blogPost.en}
              target="_blank"
              rel="noopener noreferrer"
            >
              article
            </a>{" "}
            en anglais sur les activités des Star Players si vous voulez en
            savoir plus en détail, ici je vais essayer d&apos;en parler de
            manière plus personnelle. (Il y a aussi un{" "}
            <a
              href={links.blogPost.fr}
              target="_blank"
              rel="noopener noreferrer"
            >
              autre article
            </a>
            , un peu plus ancien mais lui est en français !) <br />
            Le <span className="bold">programme Star Player</span> a été crée
            par Ubisoft pour
            <span className="italic">
              &quot;agir en tant que liaison entre Ubisoft et ses fans les plus
              passionnés&quot;
            </span>
            . Quelques fois, on est invité⸱e⸱s à des conventions - dernièrement,
            on a eu la chance d&apos;être invité⸱e⸱s au studio d&apos;Ubisoft
            Paris pour regarder la conférence{" "}
            <span className="italic">Ubisoft Forward </span> (passer la nuit
            dans un hôtel 4 étoiles est une expérience assez incroyable !), mais
            on a également une ligne de communication directe avec divers{" "}
            <span className="italic">Community Developers</span> qui reçoivent
            nos avis et les relaient aux équipes. Il nous arrive aussi
            d&apos;être impliqué⸱e⸱s directement dans les jeux. Par exemple en
            2021, on a eu la chance de créer{" "}
            <a href={links.swords.fr} target="_blank" rel="noopener noreferrer">
              deux épées
            </a>{" "}
            dans Assassin&apos;s Creed Valhalla.
          </p>
        </div>
      </div>

      <div className="pictures">
        {pictures.map((element) => (
          <PictureCard language="fr" key={element.file} picture={element} />
        ))}
      </div>
    </div>
  );
};

export default StarPlayer;
