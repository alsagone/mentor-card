const links = {
  blogPost: {
    en: "https://news.ubisoft.com/en-us/article/1dNXpWHGlOd5pgwunnDG1m/ubisoft-community-involvement-every-step-of-the-way",
    fr: "https://news.ubisoft.com/fr-fr/article/5RYyFV4VAghlyz1Gg04bXp/star-dun-jour-le-programme-starplayer-dubisoft",
  },

  swords: {
    en: "https://twitter.com/accesstheanimus/status/1433094905186947078",
    fr: "https://twitter.com/alsagone/status/1447227446328233990",
  },

  ubiExp: "https://www.youtube.com/watch?v=fzLwlP8fWf4",
  ubiFwd: {
    en: "https://www.youtube.com/watch?v=rvV4ZBx6_bo",
    fr: "https://www.youtube.com/watch?v=TkFv7525lP4",
    mirage: "https://twitter.com/mikael_creed/status/1569059156732280834",
  },
};

export default links;
